const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = [{
  mode: 'production',
  optimization: {
    minimize: false,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          format: {
            comments: false,
          },
        },
        test: /\.js(\?.*)?$/i,
        extractComments: false,
      }),
    ],
    moduleIds: 'named',
  },
  entry: {
    path: path.resolve(
      __dirname,
      'js/src/cke5-test-plugin.js',
    ),
  },
  output: {
    path: path.resolve(__dirname, 'js/dist'),
    filename: 'cke5-test-plugin.js',
    library: ['CKEditor5', 'testPlugin'],
    libraryTarget: 'umd',
    libraryExport: 'default',
  },
  plugins: [
    new webpack.BannerPlugin('cspell:disable'),
    new webpack.DllReferencePlugin({
      manifest: require(path.resolve(__dirname, './node_modules/ckeditor5/build/ckeditor5-dll.manifest.json')), // eslint-disable-line global-require, import/no-unresolved
      scope: 'ckeditor5/src',
      name: 'CKEditor5.dll',
    }),
  ],
  module: {
    rules: [{ test: /\.svg$/, use: 'raw-loader' }],
  },
}];
