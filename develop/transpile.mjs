import { createRequire } from 'module';
import { parse, resolve } from 'path';
import { mkdir, writeFile } from 'fs/promises';
import { transformFileAsync } from '@babel/core';
import { existsSync } from 'fs';

const require = createRequire(import.meta.url);
const glob = require('fast-glob');

const fileList = (
  await glob(['./ts/**/*.ts'])
).filter((path) => !path.includes('.d.ts'));

fileList.forEach(async (filePath) => {
  const babelConfig = {
    presets: ['@babel/preset-typescript'],
    env: {
      modern: {
        presets: [
          [
            '@babel/preset-env',
            {
              modules: false,
              targets: {
                browsers: ['last 2 Chrome major versions'],
              },
            },
          ],
        ],
      },
      legacy: {
        presets: [
          [
            '@babel/preset-env',
            {
              modules: false,
              targets: {
                ie: '11',
              },
            },
          ],
        ],
      },
    },
  };
  const result = await transformFileAsync(filePath, babelConfig);
  const { dir, name } = parse(filePath);
  const tempDir = resolve(process.cwd(), dir.replace('ts', 'js'));
  if (existsSync(tempDir) === false) {
    await mkdir(tempDir);
  }
  await writeFile(`${tempDir}/${name}.js`, result.code);
});
