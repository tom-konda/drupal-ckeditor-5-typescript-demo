import { Plugin } from 'ckeditor5/src/core';
export class TestPlugin extends Plugin {
  init() {
    console.log('Test plugin is initialized.');
  }

  static get pluginName() {
    return 'TestPlugin';
  }

}